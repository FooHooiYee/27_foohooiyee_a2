﻿using NUnit.Framework;
using System;
using BowlingGame;

namespace BowlingGame_Test
{
    [TestFixture()]
    public class Test
    {
        Game game;

        [SetUp()]
        public void SetupGame()
        {
            game = new Game();
        }

        void RollMany(int roll, int pins)
        {
            for (int i = 0; i < roll; i++)
            {
                game.Roll(pins);
            }
        }

        [Test()]
        public void GutterGame()
        {
            RollMany(20, 0);

            Assert.That(game.Score(), Is.EqualTo(0));
        }

        [Test()]
        public void RollOneGame()
        {
            RollMany(20, 1);

            Assert.That(game.Score(), Is.EqualTo(20));
        }

        [Test()]
        public void RollOneSpareInFirstFrame()
        {
            game.Roll(8);
            game.Roll(2);
            RollMany(18, 1);

            Assert.That(game.Score(), Is.EqualTo(29));
        }

        [Test()]
        public void RollOneStrikeInFirstFrame()
        {
            game.Roll(10);
            RollMany(18, 1);

            Assert.That(game.Score(), Is.EqualTo(30));
        }

        [Test()]
        public void RollPerfectGame()
        {
            RollMany(12, 10);

            Assert.That(game.Score(), Is.EqualTo(300));
        }

        [Test()]
        public void TypicalGame()
        {
            game.Roll(10);
            game.Roll(9); game.Roll(1);
            game.Roll(5);game.Roll(5);
            game.Roll(7); game.Roll(2);
            game.Roll(10); 
            game.Roll(10);
            game.Roll(10);
            game.Roll(9);game.Roll(0);
            game.Roll(8);game.Roll(2);
            game.Roll(9);game.Roll(1);game.Roll(10);

            Assert.That(game.Score(), Is.EqualTo(187));
        }

        [Test()]
        public void TypicalGame2()
        {
            game.Roll(5);game.Roll(1);
            game.Roll(10);
            game.Roll(5); game.Roll(5);
            game.Roll(1); game.Roll(2);
            game.Roll(10);
            game.Roll(6);game.Roll(4);
            game.Roll(10);
            game.Roll(3); game.Roll(0);
            game.Roll(0); game.Roll(1);
            game.Roll(4); game.Roll(1); 

            Assert.That(game.Score(), Is.EqualTo(102));
        }

        [Test()]
        public void TypicalGame3()
        {
            game.Roll(1); game.Roll(1);
            game.Roll(3);game.Roll(5);
            game.Roll(2); game.Roll(1);
            game.Roll(1); game.Roll(2);
            game.Roll(10);
            game.Roll(6); game.Roll(3);
            game.Roll(5);game.Roll(1);
            game.Roll(3); game.Roll(0);
            game.Roll(2); game.Roll(1);
            game.Roll(4); game.Roll(0);

            Assert.That(game.Score(), Is.EqualTo(60));
        }
    }
}
